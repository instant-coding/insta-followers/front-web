import { InstagramRepository } from '@/services/instagramRepository'
import { HttpClient } from '@/httpClient'

describe('InstagramRepository',() => {
  let instagramRepository: InstagramRepository
  let httpClient: HttpClient
  beforeEach(() => {
    httpClient = {
      post: jest.fn()
    }
    instagramRepository = new InstagramRepository(httpClient)
  })

  describe('getFollowersCount',() => {
    describe('when repository successfull',() => {
      it('returns the followers count',async () => {
        // GIVEN
        jest.spyOn(httpClient, 'post').mockResolvedValue({
          data: {
            follower: 295
          }
        })

        // WHEN
        const followersCount = await instagramRepository.getFollowersCount()

        // THEN
        expect(followersCount).toEqual(295)
      })
    })
  })
})
