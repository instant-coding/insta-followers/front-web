import InstagramStore from '@/store/InstagramStore'
import Vuex from 'vuex'
import { instagramRepository } from '@/services'

describe('InstagramStore', () => {

  let store
  const initialState = {
    followersCountState: 0
  }
  const optionsStore = {
    ...InstagramStore,
    state: { ...initialState }
  }

  describe('getters', () => {
    describe('getFollowersCount', () => {
      it('returns the followers count in state', () => {
        // GIVEN
        store = new Vuex.Store({
          ...optionsStore,
          state: {
            ...initialState,
            followersCountState: 20
          }
        })

        // WHEN
        const followers = store.getters.followersCount

        // THEN
        expect(followers).toEqual(20)

      })
    })
  })

  describe('Mutations', () => {
    describe('setFollowersCountMutation', () => {
      it('sets followers count in state', () => {
        // GIVEN
        store = new Vuex.Store({
          ...optionsStore,
          state: {
            ...initialState,
            followersCountState: 2
          }
        })

        // WHEN
        store.commit('setFollowersCountMutation', 42)

        // THEN
        expect(store.state.followersCountState).toEqual(42)
      })
    })
  })

  describe('Actions', () => {
    describe('getFollowersCount', () => {
      it('calls setFollowersCountMutation with response of repository', async () => {
        // GIVEN
        const mutations = {
          setFollowersCountMutation: jest.fn()
        }
        store = new Vuex.Store({
          ...optionsStore,
          mutations
        })
        jest.spyOn(instagramRepository, 'getFollowersCount').mockResolvedValue(42)
        jest.spyOn(mutations, 'setFollowersCountMutation')

        // WHEN
        await store.dispatch('getFollowersCount')

        // THEN
        expect(instagramRepository.getFollowersCount).toBeCalled()
        expect(instagramRepository.getFollowersCount).toBeCalledTimes(1)
        expect(mutations.setFollowersCountMutation).toBeCalledTimes(1)
        expect(mutations.setFollowersCountMutation).toBeCalledWith(initialState, 42)
      })
    })
  })

})
