import axios from 'axios'

export const httpClient = axios.create({})

export interface HttpClient {
  post<T, D>(url: string, data?: D, config?: any): Promise<T>;
}
