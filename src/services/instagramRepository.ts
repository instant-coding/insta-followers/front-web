import { HttpClient } from '@/httpClient'

export class InstagramRepository {
  readonly ENDPOINT = 'https://business.notjustanalytics.com'

  constructor(private httpClient: HttpClient) {}

  async getFollowersCount (): Promise<number> {
    const url = `${this.ENDPOINT}/analyze/getProfileInfo`
    const body = {
      profile: 'none',
      username: 'juliegri_',
      pipe: false,
      is_more: false,
      type: "free"
    }
    const { data: profileInfo } = await this.httpClient.post(url, body)
    return profileInfo.follower
  }
}
