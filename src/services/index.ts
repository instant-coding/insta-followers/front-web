import { InstagramRepository } from '@/services/instagramRepository'
import { httpClient } from '@/httpClient'

export const instagramRepository = new InstagramRepository(httpClient)
