import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators'
import { instagramRepository } from '@/services'

@Module({
  namespaced: true
})
export default class InstagramStore extends VuexModule {
  // STATE
  private followersCountState = 0;

  public get followersCount(): number {
    return this.followersCountState
  }

  @Mutation
  public setFollowersCountMutation(count: number): void {
    this.followersCountState = count;
  }

  @Action
  async getFollowersCount(): Promise<void> {
    const followersCount = await instagramRepository.getFollowersCount();
    await this.context.commit('setFollowersCountMutation', followersCount)
  }
}
