import { createStore, StoreOptions } from 'vuex'
import InstagramStore from '@/store/InstagramStore'

/* eslint-disable @typescript-eslint/no-explicit-any */
export const storeOptions: StoreOptions<any> = {
  strict: true,
  mutations: {},
  actions: {},
  modules: {
    InstagramStore
  }
}

export default createStore(storeOptions)
